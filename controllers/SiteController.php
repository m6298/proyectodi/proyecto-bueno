<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Etapa;
use yii\db;


class SiteController extends Controller
{
    public $nombrequipo;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//    VISTA DEL INICIO 
    
    public function actionIndex(){
        
//        DATA PROVIDER PARA EL SLIDER 
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT DISTINCT nomequipo FROM ciclista',
            'pagination'=>false
        ]);
        
//        DATA PROVIDER PARA EL MAPA
         $dataProvider2 = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT * FROM etapa join ciclista using (dorsal)',
            'pagination'=>false
        ]);
        
       $numEtapas = Yii::$app->db->createCommand("SELECT * FROM etapa")->queryAll();
  
        return $this->render('index',[
            'dataProvider'=>$dataProvider,
            'dataProvider2'=>$dataProvider2,
            'etapa'=>$numEtapas       
        ]);
 
    }

    
  
    
    
//    FIN VISTA DEL INICIO
//    DATAPROVIDER PARA LA VISTA DE JUGADORES
    public function actionJugadores($nombrequipo){
        
         $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                 ->where("nomequipo='$nombrequipo'"),
            'pagination'=>false
        ]);
         
        return $this->render('jugadores',[
            'dataProvider'=>$dataProvider,
            'equipo'=>$nombrequipo
        ]);
    }
    
//    FIN LISTA DE JUGADORES
//    CONSULTAS PARA LAS FICHAS DE LOS JUGADORES
    public function actionFicha($dorsal){
        
              
        $etapasGanadas = Yii::$app->db->createCommand("SELECT Count(*) FROM etapa where dorsal='$dorsal'")
                ->queryAll();
        
        
        
        return $this->render("fichaJugador",[
            'etapasGandas'=>$etapasGanadas,
            'dorsal'=>$dorsal
        ]);
    }
    
//    FIN FICHA DE JUGADORES
    
//    CONSULTAS PARA LAS ETAPAS
    
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    

    
}
