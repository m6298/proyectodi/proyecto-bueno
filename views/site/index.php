<?php

/* @var $this yii\web\View */

$this->title = 'Ciclistas';
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\ListView;
use kartik\popover\PopoverX;

?>



    <?php 
    $this->registerJs("$('#recipeCarousel').carousel({
        interval: 9000000
        })"
        ,View::POS_READY);
        
    $this->registerJs(" $('.carousel .carousel-item').each(function(){
            var minPerSlide = 2;
            var next = $(this).next();
            if (!next.length) {
            next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i=0;i<minPerSlide;i++) {
                next=next.next();
                if (!next.length) {
                        next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
              }
        });",
          View::POS_READY);
    
    $this->registerCss("@media (max-width: 768px) {
            .carousel-inner .carousel-item > div {
                display: none;
            }
            .carousel-inner .carousel-item > div:first-child {
                display: block;
            }
        }

        .carousel-inner .carousel-item.active,
        .carousel-inner .carousel-item-next,
        .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* display 3 */
        @media (min-width: 768px) {

            .carousel-inner .carousel-item-right.active,
            .carousel-inner .carousel-item-next {
              transform: translateX(33.333%);
            }

            .carousel-inner .carousel-item-left.active, 
            .carousel-inner .carousel-item-prev {
              transform: translateX(-33.333%);
            }
        }

        .carousel-inner .carousel-item-right,
        .carousel-inner .carousel-item-left{ 
          transform: translateX(0);
        }"
            );
    ?>
    
<div class="site-index">
      
    <h1 align="center" style="margin-bottom: 20px; font-family: roboto; font-weight: bold">Equipos vuelta España</h1>
    <p align="center">Selecciona el equipo que desea ver</p>
    
    <div class="row mx-auto my-auto" >
        <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel" style="margin-bottom: 20px">
            <div class="carousel-inner w-100" role="listbox">
                <div class="carousel-item active">
                    <div class="col-md-4">
                        <div class="card card-body"style="padding-bottom: 6px">
                           <?= Html::img('@web/images/equipos/logo.png',['width'=>'100%','height'=>'300px']) ?>
                            <p id="nomequipo"align="center" style="margin-top: 20px; margin-bottom: 5px ;font-size: 20px">
                                Vuelta España
                            </p>
                        </div>
                    </div>
                </div>
                
    <!--OBJETOS PARA LOS EQUIPOS-->
        
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        #'itemView' => '_jugadores',
                        #'options'=>['class'=>'col-sm-6 col-md-4'],
                        #'itemView'=>['class'=>'card tarjeta']
                        'layout'=>"{items}",
                        'itemView'=> function($model){

                    ?>
    
                <div class="carousel-item">
                    <div class="col-md-4">
                        <div class="card card-body" style="padding-bottom: 6px">
                            <?= Html::a(Html::img('@web/images/equipos/'.$model['nomequipo'].'.jpg',['width'=>'100%','height'=>'300px']),
                                    ['site/jugadores','nombrequipo'=>$model['nomequipo']],
                                    ['class'=>'img-fluid']) ?>
                            <p id="nomequipo" align="center" style="margin-top: 20px; margin-bottom: 5px ;font-size: 20px">
                                <?= $model['nomequipo'] ?>
                            </p>
                        </div>
                    </div>
                </div>
                   
                    <?php 
                        }
                        ]);
                    ?>
    
                    
            </div>
            <a class="carousel-control-prev w-auto" href="#recipeCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">anterior</span>
            </a>
            <a class="carousel-control-next w-auto" href="#recipeCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">siguiente</span>
            </a>
            
        </div>
    </div> 
  
    
    <div align="center">
        
        <h1 style="margin-bottom: 20px; font-family: roboto; font-weight: bold">Mapa vuelta España</h1>
        
        <p style="margin-bottom: 5px">Clica cobre la etapa que quiere consultar</p>
        
        
        
        
    <?= Html::img('@web/images/mapa/mapa.jpg',['usemap'=>'#mapa']); ?>
    
        
        <map name="mapa">
        
            
            
            <area alt="1" id="1" shape="rect" coords="273,586,290,601" type="button" data-toggle="modal" data-target="#etapa1  " data-whatever="@getbootstrap" >
            <area alt="2" shape="rect" coords="219,502,236,517" type="button" data-toggle="modal" data-target="#etapa2  " data-whatever="@getbootstrap">
            <area alt="3" shape="rect" coords="187,543,204,558" type="button" data-toggle="modal" data-target="#etapa3  " data-whatever="@getbootstrap">
            <area alt="4" shape="rect" coords="262,510,278,525" type="button" data-toggle="modal" data-target="#etapa4  " data-whatever="@getbootstrap">
            <area alt="5" shape="rect" coords="350,541,367,556" type="button" data-toggle="modal" data-target="#etapa5  " data-whatever="@getbootstrap">
            <area alt="6" shape="rect" coords="430,495,447,510" type="button" data-toggle="modal" data-target="#etapa6  " data-whatever="@getbootstrap">
            <area alt="7" shape="rect" coords="361,466,377,480" type="button" data-toggle="modal" data-target="#etapa7  " data-whatever="@getbootstrap">
            <area alt="8" shape="rect" coords="212,439,230,454"  type="button" data-toggle="modal" data-target="#etapa8  " data-whatever="@getbootstrap">
            <area alt="9" shape="rect" coords="198,330,214,345" type="button" data-toggle="modal" data-target="#etapa9  " data-whatever="@getbootstrap">
            <area alt="10" shape="rect" coords="142,271,162,287" type="button" data-toggle="modal" data-target="#etapa10  " data-whatever="@getbootstrap">
            <area alt="11" shape="rect" coords="107,213,128,228" type="button" data-toggle="modal" data-target="#etapa11  " data-whatever="@getbootstrap">
            <area alt="12" shape="rect" coords="54,81,73,95" type="button" data-toggle="modal" data-target="#etapa12  " data-whatever="@getbootstrap">
            <area alt="13" shape="rect" coords="229,139,249,154" type="button" data-toggle="modal" data-target="#etapa13  " data-whatever="@getbootstrap">
            <area alt="14" shape="rect" coords="181,142,201,157" type="button" data-toggle="modal" data-target="#etapa14  " data-whatever="@getbootstrap">
            <area alt="15" shape="rect" coords="207,94,227,108" type="button" data-toggle="modal" data-target="#etapa15  " data-whatever="@getbootstrap">
            <area alt="16" shape="rect" coords="270,74,290,88" type="button" data-toggle="modal" data-target="#etapa16  " data-whatever="@getbootstrap">
            <area alt="17" shape="rect" coords="352,100,373,115" type="button" data-toggle="modal" data-target="#etapa17  " data-whatever="@getbootstrap">
            <area alt="18" shape="rect" coords="429,214,448,228" type="button" data-toggle="modal" data-target="#etapa18  " data-whatever="@getbootstrap">
            <area alt="19" shape="rect" coords="546,212,565,227" type="button" data-toggle="modal" data-target="#etapa19  " data-whatever="@getbootstrap">
            <area alt="20" shape="rect" coords="550,61,571,77" type="button" data-toggle="modal" data-target="#etapa20  " data-whatever="@getbootstrap">
            <area alt="21" shape="rect" coords="277,277,297,291" type="button" data-toggle="modal" data-target="#etapa21  " data-whatever="@getbootstrap">

        
        
        </map>
        
        <p style="font-size: 12px">El mapa no corresponden con los puntos de salida reales</p>
    </div>
    
    <?=  ListView::widget([
        'dataProvider' => $dataProvider2,
        #'itemView' => '_jugadores',
        #'options'=>['class'=>'col-sm-6 col-md-4'],
        #'itemView'=>['class'=>'card tarjeta']
        'layout'=>"{items}",
        'itemView'=> function($model){

    ?>
    
        <!--MODAL ETAPA 1--> 
        <div class="modal fade" id="etapa<?php echo $model['numetapa'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Etapa seleccionada 
                    <?= $model['numetapa']; ?> 
                </h5>
               
              </div>
              <div class="modal-body" align="center">
                <!--LO QUE APARECE DENTRO DEL MODAL-->
                <p> El lugar de salida es: <?= $model['salida'] ?></p>
                <p> El lugar de llegada es: <?= $model['llegada'] ?> </p>
                <p> La cantidad de Kilometros recorridos son: <?= $model['kms'] ?></p>
                <p> El ganador de la etapa fue : <?= $model['nombre'] ?> </p>
                <p> Participando en el equipo de: <?= $model['nomequipo'] ?> </p>
                <?= Html::a('ficha del jugador',['site/ficha','dorsal'=>$model['dorsal']],['class'=>'btn btn-dark']) ?>
                <?= Html::a('Ciclistas del equipo',['site/jugadores','nombrequipo'=>$model['nomequipo']],['class'=>'btn btn-dark']) ?>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                
              </div>
            </div>
          </div>
        </div>       

     <?php 
        }
        ]);
    ?>
    
</div>                
   

