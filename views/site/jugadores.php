<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\widgets\ListView;
?>


<div class="site-index">
    <h1 align="center" style="font-family: roboto; font-weight: bold">Ciclistas de <?= $equipo ?></h1>
    <div class="row">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        #'itemView' => '_jugadores',
        #'options'=>['class'=>'col-sm-6 col-md-4'],
        #'itemView'=>['class'=>'card tarjeta']
        'layout'=>"{items}",
        'viewParams'=> [
            
        ],
        'itemView'=> function($model){

    ?>
    
        <div class="col-sm-6 col-md-4" style="margin-top: 20px">
                <div class="card alturaminimaJugadores">
                    <div class="card-body tarjeta">
                        <p #id="juga">
                            <?php echo Html::img('@web/images/ciclistas/'.$model->dorsal.'.jpg',['width'=>'100%','height'=>'300px','class'=>'juga']); ?>
                        </p>
                        <p>
                            Nombre: <?= $model->nombre?>
                        </p>
                        <p>
                            Dorsal: <?= $model->dorsal ?>
                        </p>
                        <?= Html::a('Ver ficha del jugador',['site/ficha','dorsal'=>$model->dorsal],['class'=>'btn btn-dark']) ?>
                    </div>
                </div>
           
        </div>
        
        <?php 
            }
            ]);
        ?>
   </div>              
</div>
